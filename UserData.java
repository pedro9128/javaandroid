package br.com.doctum.optativa.aulamobile;

import java.util.ArrayList;

public class UserData {

    private String nome;
    private String email;
    private String telefone;
    private int idade;
    private ArrayList<UserData> userList;

    public UserData() {
        this.nome = new String();
        this.email = new String();
        this.telefone = new String();
        this.idade = 0;
        userList = new ArrayList<>();
    }

    public UserData(String nome, String email, String telefone, int idade) {
        this();
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
        this.idade = idade;
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public ArrayList<UserData> getUserList() {
        return userList;
    }

    public void setUserList(ArrayList<UserData> userList) {
        this.userList = userList;
    }

    public void insert(String nome, String email, String telefone, int idade) {
        UserData userData = new UserData();
        userData.nome = nome;
        userData.email = email;
        userData.telefone = telefone;
        userData.idade = idade;
        userList.add(userData);
    }
    public void print() {
        for(int i = 0; i < userList.size() ; i++)
            System.out.println(this.userList.get(i).getNome());

    }
}
