package br.com.doctum.optativa.aulamobile;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class UserRegisterActivity extends LifeCycle {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userregister);
        Button button = findViewById(R.id.buttonRegister);
        button.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                UserData userData = new UserData();
                EditText editTextUser = findViewById(R.id.registerUser);
                String nome = editTextUser.getText().toString();
                EditText editTextEmail = findViewById(R.id.registerEmail);
                String email = editTextEmail.getText().toString();
                EditText editTextTelefone = findViewById(R.id.registerTelefone);
                String telefone = editTextTelefone.getText().toString();
                EditText editTextIdade = findViewById(R.id.registerIdade);
                int idade = Integer.parseInt(editTextIdade.getText().toString());
                userData.insert(nome, email, telefone, idade);
                System.out.println(userData.getUserList().size());
                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                startActivity(intent);
            }
        });
    }
}